## [1.11.3](http://bitbucket.org/idatasuite/idata-suite-users/compare/v1.11.2...v1.11.3) (2022-09-27)


### Reverts

* Revert "#feat(core): aplica endpoints de grupos de usuarios para validación de administradores" ([dbab92c](http://bitbucket.org/idatasuite/idata-suite-users/commits/dbab92cf280283efaf8539fb571c161d8c35ba2b))
* Revert "#feat(core): aplica endpoints de grupos de usuarios para validación de administradores" ([7468ca6](http://bitbucket.org/idatasuite/idata-suite-users/commits/7468ca6a63c6b666d2fa1c9c7f25bffe0b31dfeb))
* Revert "#feat(core): aplica endpoints de grupos de usuarios para validación de administradores" ([5abee93](http://bitbucket.org/idatasuite/idata-suite-users/commits/5abee93061bc02e4852e031b70b4b85d7e97ccff))

## [1.11.2](http://bitbucket.org/idatasuite/idata-suite-users/compare/v1.11.1...v1.11.2) (2021-11-16)


### Bug Fixes

* **core:** resuelve alta de usuarios en s2s ([27a8ce7](http://bitbucket.org/idatasuite/idata-suite-users/commits/27a8ce73e122cae7c4e9694e90873a930745cd8b))
* **core:** resuelve alta de usuarios en s2s ([238651e](http://bitbucket.org/idatasuite/idata-suite-users/commits/238651ebda11aec9a680866bd1541bc6d4a610b6))

## [1.11.1](http://bitbucket.org/idatasuite/idata-suite-users/compare/v1.11.0...v1.11.1) (2021-04-16)


### Bug Fixes

* **core:** resuelve alta de usuarios preexistentes en cognito ([2d1897a](http://bitbucket.org/idatasuite/idata-suite-users/commits/2d1897a4fd1141d1fd7b1f6fbe88a78212d03db6))
* **core:** resuelve alta de usuarios preexistentes en cognito ([46abcb1](http://bitbucket.org/idatasuite/idata-suite-users/commits/46abcb172eec138eef38d36649da0ca1f1a97a8e))

# [1.11.0](http://bitbucket.org/idatasuite/idata-suite-users/compare/v1.10.5...v1.11.0) (2021-03-15)


### Bug Fixes

* **core:** arregla bug en composer install ([5a4adb3](http://bitbucket.org/idatasuite/idata-suite-users/commits/5a4adb316350693a346eb1394e487428bdd1b9ad))
* **core:** arregla bug en composer install ([b04df99](http://bitbucket.org/idatasuite/idata-suite-users/commits/b04df99dfc82ec390df423d0d02e6cad9ba52e7a))
* **core:** remueve xdebug del repo y actualiza php a 7.4 ([5ab26ce](http://bitbucket.org/idatasuite/idata-suite-users/commits/5ab26ce1153b0d25cebe72d515ad6b4bc1421bfc))


### Features

* **core:** implementa función para registros en batch ([11aa158](http://bitbucket.org/idatasuite/idata-suite-users/commits/11aa15804801d19af9edd18827500bd3ae8a7a4f))
* **core:** implementa validador para registros en batch ([ef23ee2](http://bitbucket.org/idatasuite/idata-suite-users/commits/ef23ee230ed4436c6eafbfb9b219207b9ff30b94))
* **core:** implementa validador para registros en batch ([9434769](http://bitbucket.org/idatasuite/idata-suite-users/commits/9434769a9eb8862393e248f2b22f8d90f34da2dd))

## [1.10.5](http://bitbucket.org/idatasuite/idata-suite-users/compare/v1.10.4...v1.10.5) (2021-03-15)


### Bug Fixes

* **core:** agrega campos faltantes al composer.json ([5f50bd6](http://bitbucket.org/idatasuite/idata-suite-users/commits/5f50bd622c265d28757ffafba69ed923b29fffe5))
* **core:** agrega campos faltantes al composer.json ([11cd910](http://bitbucket.org/idatasuite/idata-suite-users/commits/11cd9100d336365b99e05585d31738d747e402a1))

## [1.10.4](http://bitbucket.org/idatasuite/idata-suite-users/compare/v1.10.3...v1.10.4) (2021-03-15)


### Bug Fixes

* **core:** resuelve deprecaciones de composer 2.0 ([21f67e1](http://bitbucket.org/idatasuite/idata-suite-users/commits/21f67e1df72278338104fac4b9ce157671c49605))
* **core:** resuelve deprecaciones de composer 2.0 ([ab7dffa](http://bitbucket.org/idatasuite/idata-suite-users/commits/ab7dffa1f71bca9dbf0a8d9ad6846f63e3d2e036))

## [1.10.3](http://bitbucket.org/idatasuite/idata-suite-users/compare/v1.10.2...v1.10.3) (2020-08-12)


### Bug Fixes

* **core:** arregla posibles errores cuando el mail informado para resetear contiene mayusculas ([712c3db](http://bitbucket.org/idatasuite/idata-suite-users/commits/712c3db84b45e865509a15bb976374eb1b6bbbee))

## [1.10.2](http://bitbucket.org/idatasuite/idata-suite-users/compare/v1.10.1...v1.10.2) (2020-08-12)


### Bug Fixes

* **core:** arregla posibles errores cuando el mail informado para resetear contiene mayusculas ([5c9c6bd](http://bitbucket.org/idatasuite/idata-suite-users/commits/5c9c6bdc7b448e76b81cfc072057294ec09f8df8))
* **core:** arregla posibles errores cuando el mail informado para resetear contiene mayusculas ([d933921](http://bitbucket.org/idatasuite/idata-suite-users/commits/d93392138f0e06ae0d13c23e90e59ebd34a876bf))

## [1.10.1](http://bitbucket.org/idatasuite/idata-suite-users/compare/v1.10.0...v1.10.1) (2020-08-06)


### Bug Fixes

* **core:** deshabilita el envío de mails en el resteo viejo. ([8cdc8d8](http://bitbucket.org/idatasuite/idata-suite-users/commits/8cdc8d8906d6f66c1384688114aa659019810313))
* **core:** deshabilita el envío de mails en el resteo viejo. ([fe2c8ce](http://bitbucket.org/idatasuite/idata-suite-users/commits/fe2c8cea11157929faab956d51642257159a28a6))

# [1.10.0](http://bitbucket.org/idatasuite/idata-suite-users/compare/v1.9.0...v1.10.0) (2020-06-11)


### Bug Fixes

* **core:** modifica link dinamico para reconcer USERNAME en el reset ([e1c1f16](http://bitbucket.org/idatasuite/idata-suite-users/commits/e1c1f1640cd6a636c0c00598bbb522bcff957f35))


### Features

* **core:** implementa autenticación en endpoints de reset ([75ba52a](http://bitbucket.org/idatasuite/idata-suite-users/commits/75ba52a2d345f865d646acaea287b1edd5770dea))
* **core:** implementa nuevo flujo de envios de mails desde Suite-Core ([a8796fa](http://bitbucket.org/idatasuite/idata-suite-users/commits/a8796faac159808e38a6b84bfe0a8f0db86446b6))
* **core:** implementa nuevo reseteo de contraseña via web solamente ([803209c](http://bitbucket.org/idatasuite/idata-suite-users/commits/803209c48ea3789f1e444fce4d0782975ec3c96f))
* **core:** implementa REPLY_TO en emails desde env REPLY_TO_ADDRESS ([3108a00](http://bitbucket.org/idatasuite/idata-suite-users/commits/3108a0026fb079ac3e805f538fec753bbadcfcca))
* **core:** implementa URL dinamica de Firebase con reset=true ([cf02d56](http://bitbucket.org/idatasuite/idata-suite-users/commits/cf02d5661db7275c62e5c6964e8a69494e2b384e))
* **core:** implenta nueva URL para resetPassword ([47f1d6e](http://bitbucket.org/idatasuite/idata-suite-users/commits/47f1d6ea93aa44c62feda00c7583707011d9b81b))
* **core:** reincorpora metodo viejo de reseteo para mantener compatibilidad ([d625db4](http://bitbucket.org/idatasuite/idata-suite-users/commits/d625db4f944fe7403f4d2715166bd0716e66d700))

# [1.9.0](http://bitbucket.org/idatasuite/idata-suite-users/compare/v1.8.1...v1.9.0) (2020-06-05)


### Features

* **core:** implementa REPLY_TO en emails desde env REPLY_TO_ADDRESS ([4f47140](http://bitbucket.org/idatasuite/idata-suite-users/commits/4f47140b5517551f2f54b0edb025fd1c9506de84))
* **core:** implementa REPLY_TO en emails desde env REPLY_TO_ADDRESS ([9af1bfe](http://bitbucket.org/idatasuite/idata-suite-users/commits/9af1bfeb67571037f9d3e809c06fc8f0d88181f2))

## [1.8.1](http://bitbucket.org/idatasuite/idata-suite-users/compare/v1.8.0...v1.8.1) (2020-04-13)


### Bug Fixes

* **core:** resuelve problema en reseteo de contraseña ([ce1ec02](http://bitbucket.org/idatasuite/idata-suite-users/commits/ce1ec02634c1fa63ed42545138e4a219658a0eed))

# [1.8.0](http://bitbucket.org/idatasuite/idata-suite-users/compare/v1.7.0...v1.8.0) (2020-04-09)


### Features

* **core:** implementa registro de nuevas asociaciones de UF en la DB ([af0856b](http://bitbucket.org/idatasuite/idata-suite-users/commits/af0856bd9238ee17870ff20de80498e4c7bf7a59))
* **core:** registra asociaciones de UF en la DB ([f145349](http://bitbucket.org/idatasuite/idata-suite-users/commits/f145349bca16b44ded2a075aadbc0ef5029264df))

# [1.7.0](http://bitbucket.org/idatasuite/idata-suite-users/compare/v1.6.0...v1.7.0) (2020-04-02)


### Bug Fixes

* **core:** remueve archivos .idea del repo ([7bd98f2](http://bitbucket.org/idatasuite/idata-suite-users/commits/7bd98f2671328269c3cc49541858adfb6733b420))


### Features

* **core:** implementa nuevo flujo de envios de mails desde Suite-Core ([5c2d541](http://bitbucket.org/idatasuite/idata-suite-users/commits/5c2d5411d896880b68a0daee0d777e0145247b5b))
* **core:** implementa resetPassword junto a varias optimizaciones ([62264f6](http://bitbucket.org/idatasuite/idata-suite-users/commits/62264f674909a23644c5fea946cd9f7a34ecf9fa))

# [1.6.0](http://bitbucket.org/idatasuite/idata-suite-users/compare/v1.5.2...v1.6.0) (2020-03-25)


### Bug Fixes

* **core:** arregla envio de email en reset de contraseña ([84515df](http://bitbucket.org/idatasuite/idata-suite-users/commits/84515dfcdecca5ebb7f1ec153b590894be561d1f))
* **core:** arregla seteo de usuario en reseteo de contraseña ([9c62daa](http://bitbucket.org/idatasuite/idata-suite-users/commits/9c62daa9e34ea400d2e4408e2404c6874ab4ca7c))
* **core:** arregla seteo de usuario en reseteo de contraseña ([feb5e2a](http://bitbucket.org/idatasuite/idata-suite-users/commits/feb5e2a1804b3bd65815ddc3d064475cec22a168))
* **docs:** aclara resolución contraseñas cognito ([f17b0e9](http://bitbucket.org/idatasuite/idata-suite-users/commits/f17b0e9a2dcabcec8f8896604b93bbb3ccdefabe))


### Features

* **core:** implementa controladora Firebase, y metodo getlink ([8001ccf](http://bitbucket.org/idatasuite/idata-suite-users/commits/8001ccf0177c9ded3569ddbd3ede16a902b918ab))
* **core:** implementa resetPassword junto a varias optimizaciones ([da7cf21](http://bitbucket.org/idatasuite/idata-suite-users/commits/da7cf218cc5fcf858b589da9c01ab6cb04d20d53))

## [1.5.2](http://bitbucket.org/idatasuite/idata-suite-users/compare/v1.5.1...v1.5.2) (2020-03-12)


### Bug Fixes

* **core:** arregla devolución de excepción ante error de alta en Cognito ([862a311](http://bitbucket.org/idatasuite/idata-suite-users/commits/862a3119df6d012972fea7705c88ccb68fd30385))
* **core:** arregla error de contraseña alfabetica y mejora respuesta ([d7dd558](http://bitbucket.org/idatasuite/idata-suite-users/commits/d7dd5585e31ac0ca986bb940531bd19aa06b6abf))
* **core:** arregla generación de contraseñas para matchear condiciones Cognito ([af45252](http://bitbucket.org/idatasuite/idata-suite-users/commits/af45252ad78b70b75c4cb9b3f8d6cdec99a95979))

## [1.5.1](http://bitbucket.org/idatasuite/idata-suite-users/compare/v1.5.0...v1.5.1) (2020-03-11)


### Bug Fixes

* **core:** ignora archivos de sonarlint y workspace ([4fd7551](http://bitbucket.org/idatasuite/idata-suite-users/commits/4fd7551da846fd9f81a93043764edbf167ce28bf))

# [1.5.0](http://bitbucket.org/idatasuite/idata-suite-users/compare/v1.4.0...v1.5.0) (2020-03-10)


### Features

* **core:** implementa la asociacion de multiples unidades funcionales a un mismo usuario ([bd311e9](http://bitbucket.org/idatasuite/idata-suite-users/commits/bd311e9916592205365d48dcc95b288436ac47cc))
* **docs:** cambia numero de veriones a los correctos ([84ff2f2](http://bitbucket.org/idatasuite/idata-suite-users/commits/84ff2f27e41d1701f2b9bb605daa3af632ea9fcf))

# [1.4.0](http://bitbucket.org/idatasuite/idata-suite-users/compare/v1.3.0...v1.4.0) (2020-03-10)


### Features

* **core:** implementa la asociacion de multiples unidades funcionales a un mismo usuario ([aca6ec0](http://bitbucket.org/idatasuite/idata-suite-users/commits/aca6ec045156e08f419c86c546cf7db9b0320e56))

# [1.3.0](http://bitbucket.org/idatasuite/idata-suite-users/compare/v1.2.2...v1.3.0) (2020-03-10)


### Features

* **core:** agrega try/catch para devolver cualquier excepción en cualquier momento ([e23a4f8](http://bitbucket.org/idatasuite/idata-suite-users/commits/e23a4f806062a09db6a8c4e756b061975717959e))
* **core:** permite la asociación de multiples UF para un mismo usuario ([aea7305](http://bitbucket.org/idatasuite/idata-suite-users/commits/aea73053c60ecffeb1a07b3d4eacee7ed73462a6))

## [1.2.2](http://bitbucket.org/idatasuite/idata-suite-users/compare/v1.2.1...v1.2.2) (2020-03-04)


### Bug Fixes

* **core:** agrega variable de entorno para s2s ([e0a58e7](http://bitbucket.org/idatasuite/idata-suite-users/commits/e0a58e75605c0cf01fb62515b881a11fcdc8063e))

## [1.2.1](http://bitbucket.org/idatasuite/idata-suite-users/compare/v1.2.0...v1.2.1) (2020-03-04)


### Bug Fixes

* **core:** encodea datos en json para el body del request a Suite | Core ([0ad8a2a](http://bitbucket.org/idatasuite/idata-suite-users/commits/0ad8a2a08f939b174868acf579d0029920ac1857))
* **core:** encodea datos en json para el body del request a suite core ([abb7eb1](http://bitbucket.org/idatasuite/idata-suite-users/commits/abb7eb143696750a7bac66565ce94c051e933919))

# [1.2.0](http://bitbucket.org/idatasuite/idata-suite-users/compare/v1.1.1...v1.2.0) (2020-03-02)


### Bug Fixes

* **core:** arregla problemas con el composer install en Elastic Beanstalk ([11b47bd](http://bitbucket.org/idatasuite/idata-suite-users/commits/11b47bd253c13f4ba760ad72f04506cc74424092))


### Features

* **debug:** habilita y configura xdebug en elasticbeanstalk ([c3e8fbe](http://bitbucket.org/idatasuite/idata-suite-users/commits/c3e8fbe4bbe0404bbe773abf56645c2aa1190a6c))
* **debug:** habilita y configura xdebug en elasticbeanstalk ([1c5eb02](http://bitbucket.org/idatasuite/idata-suite-users/commits/1c5eb02b40bd9279a5339135a82590a00fcab3a6))

## [1.1.1](http://bitbucket.org/idatasuite/idata-suite-users/compare/v1.1.0...v1.1.1) (2020-02-28)


### Bug Fixes

* **core:** arregla body del request de alta de usuarios a cake ([48e69ac](http://bitbucket.org/idatasuite/idata-suite-users/commits/48e69acfa3517451e7c053fed1f69651939bd204))
* **core:** arregla body del request de alta de usuarios a cake ([d1e6041](http://bitbucket.org/idatasuite/idata-suite-users/commits/d1e60417dd89ff1993741281535085448863b12a))

# [1.1.0](http://bitbucket.org/idatasuite/idata-suite-users/compare/v1.0.1...v1.1.0) (2020-02-28)


### Bug Fixes

* **build:** ignora sonarqube scanner ([108c916](http://bitbucket.org/idatasuite/idata-suite-users/commits/108c916337c868f074fe60481834c7faf488d585))
* **core:** arregla llamada jsonresponse ([51c45df](http://bitbucket.org/idatasuite/idata-suite-users/commits/51c45df72190344d96ce5be16bf1e93c5e7aaca0))
* **core:** arregla problemas con el composer install en Elastic Beanstalk ([9d0313d](http://bitbucket.org/idatasuite/idata-suite-users/commits/9d0313d9e5b439132522350d3f63778a7e1b2d9f))
* **core:** arregla respuesta de administrationName ([3626b94](http://bitbucket.org/idatasuite/idata-suite-users/commits/3626b94644720ff48162bfcd277604c466de7c4c))
* **core:** implementa pack para deploy en apache ([90b2f7f](http://bitbucket.org/idatasuite/idata-suite-users/commits/90b2f7fe2da7478b68ae3c8da49574d969986d2d))
* **core:** migra las url a variables de entorno ([4b8548e](http://bitbucket.org/idatasuite/idata-suite-users/commits/4b8548ec25c79a6f84643fa8260018047ab42bcd))
* **core:** suma códigos de respuesta, adiciona ruta para reset ([d66ee31](http://bitbucket.org/idatasuite/idata-suite-users/commits/d66ee31bbefe2737860a1a4c6c32e5a0f16f6b72))


### Features

* **core:** implementa nueva api de alta de usuarios y reset de claves ([f2392d4](http://bitbucket.org/idatasuite/idata-suite-users/commits/f2392d4d2068d612492a1b9f6924c39d0d7feed9))
* **core:** implementa un data transfer object para validar el json de alta ([c8c5aa8](http://bitbucket.org/idatasuite/idata-suite-users/commits/c8c5aa8cde6ab525f77f654f6a4f3ade3c03d524))

## [1.0.1](http://bitbucket.org/idatasuite/idata-suite-users/compare/v1.0.0...v1.0.1) (2020-01-22)


### Bug Fixes

* **docs:** corrige nombre del proyecto y agrega descripción a package.json ([745b50a](http://bitbucket.org/idatasuite/idata-suite-users/commits/745b50aa4e7063c74a23a273b73aa4c14ada7793))

# 1.0.0 (2020-01-22)


### Features

* inicializa repo con pipeline de sonar-scanner y semantic release ([1942e69](http://bitbucket.org/idatasuite/idata-suite-users/commits/1942e69305d08b84ae964a17c910f24a4c4ba342))
