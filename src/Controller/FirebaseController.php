<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;

class FirebaseController extends AbstractController
{
    private $front_url;
    private $firebase_key;

    public function __construct(string $firebase_key, string $front_url)
    {
        $this->firebase_key = $firebase_key;
        $this->front_url = $front_url;
    }
    public function getResetLink($email, $urlData, $type = null, $password)
    {

        if ($type === 'administrator') {
            $url = "https://administradores.page.link/?link=".$urlData."&apn=ar.com.idata.suite.administradores&ibi=ar.com.idata.suite.administradores";
        } else {
            $url = "https://copropietario.page.link/?link=".$urlData."?username=".$email."&apn=ar.com.idata.suite.copropietario&ibi=ar.com.idata.suite.copropietario";
        }
        return $this->_getlink($url, $type);
    }

    private function _getlink($url, $type = null)
    {

        $http =  HttpClient::create();
        $fbUrl = 'https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key='.$this->firebase_key;

        $body = array(
            "longDynamicLink" =>$url
        );
        $response = $http->request('POST',$fbUrl,[
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
            ],
            'body' => json_encode($body)
        ]);

        $url = json_decode( $response->getContent(), true);
        return $url['shortLink'];
    }

}
