<?php

namespace App\Controller;

use App\Entity\Users;
use App\Validator\BatchUserRequest;
use App\Validator\LoginRequest;
use App\Validator\NewTokenRequest;
use App\Validator\ResetPasswordRequest;
use App\Validator\NewUserRequest;
use Doctrine\ORM\EntityManagerInterface;
use http\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class UsersController extends AbstractController
{
    private $usersRepository;
    private $front_url;
    private $s2s_url;
    private $s2s_token;

    public function __construct($front_url, $s2s_url, $s2s_token, EntityManagerInterface $entityManager)
    {
        $this->usersRepository = $entityManager->getRepository(Users::class);
        $this->front_url = $front_url;
        $this->s2s_url = $s2s_url;
        $this->s2s_token = $s2s_token;
    }

    /**
     * @Route("/", name="index", methods={"GET"})
     * @return JsonResponse
     */
    public function index()
    {
        return new JsonResponse(['No tienes privilegios para acceder a este contenido!'], 403);
    }

    /**
     * @Route("/addUser", name="user_add", methods={"POST"})
     * @param NewUserRequest $newUserData
     * @param CognitoController $cognitoController
     * @param MailerController $mailerController
     * @return JsonResponse
     */
    public function addUser(NewUserRequest $newUserData, CognitoController $cognitoController)
    {
        $cognitoUser = $cognitoController->getCognitoUsers($newUserData->email());
        try {
            $password = $this->usersRepository->generateUid($newUserData->email());

            switch(true){
                // Nuevo Usuario
                case (!count($cognitoUser['Users']) && !$newUserData->type()):
                    $cognitoUser = $cognitoController->createCognitoUser($newUserData->email(), $password);
                    $cognitoController->checkCredentials($newUserData->email(), $password);
                    $newUserEntity = $this->usersRepository->SaveUsers($newUserData->email(), $password, "invite", $newUserData->functional_unit_id());
                    $cognitoUserId = $cognitoUser['User']['Attributes'][0]['Value'];
                    if (!$newUserData->type()) {
                        $this->saveCoreApiUser($newUserData, $cognitoUserId, $newUserEntity->getTypeEmail(), $password);
                    } else {
                        $this->saveCoreApiUser($newUserData, $cognitoUserId, $newUserData->type(), $password);
                    }
                    break;
                // Asociación de UF o Alta de usuario preexistente
                case (count($cognitoUser['Users']) && !$newUserData->type()):
                    $cognitoUserId = $cognitoUser['Users']['0']['Attributes'][0]['Value'];
                    $newUserEntity = $this->usersRepository->SaveUsers($newUserData->email(), $password, "preexisting", $newUserData->functional_unit_id());
                    $this->saveCoreApiUser($newUserData, $cognitoUserId, $newUserEntity->getTypeEmail(), $password);
                    break;
                // Alta Silenciosa
                case (!count($cognitoUser['Users']) && $newUserData->type() === "silent"):
                    $cognitoUser = $cognitoController->createCognitoUser($newUserData->email(), $password);
                    $cognitoController->checkCredentials($newUserData->email(), $password);
                    $this->usersRepository->SaveUsers($newUserData->email(), $password, $newUserData->type(), $newUserData->functional_unit_id());
                    $cognitoUserId = $cognitoUser['User']['Attributes'][0]['Value'];
                    $this->saveCoreApiUser($newUserData, $cognitoUserId, $newUserData->type(), $password);
                    break;
                // Reinvitación
                case (count($cognitoUser['Users']) && $newUserData->type() === "reinvite"):
                    $cognitoUserId = $cognitoUser['Users']['0']['Attributes'][0]['Value'];
                    $this->usersRepository->SaveUsers($newUserData->email(), $password, $newUserData->type(), $newUserData->functional_unit_id());
                    $this->saveCoreApiUser($newUserData, $cognitoUserId, $newUserData->type(), $password);
                    break;
            }
            return new JsonResponse(['Estado' => 'OK', 'Descripción' => 'Operación realizada correctamente!'], 202);
        } catch (Exception $exception) {
            return new JsonResponse(['Estado' => 'ERROR', 'Descripción' => $exception->getMessage()], 400);
        }
    }


    /**
     * @Route("/sendResetRequest", name="user_sendReset", methods={"POST"})
     * @param Request $request
     * @param CognitoController $cognitoController
     * @param MailerController $mailerController
     * @param AuthenticationController $authenticationController
     * @param FirebaseController $firebaseController
     * @return JsonResponse
     */
    public function sendResetRequest(Request $request, CognitoController $cognitoController, MailerController $mailerController, AuthenticationController $authenticationController, FirebaseController $firebaseController)
    {
        $token = $request->headers->get('Authorization');
        $authenticationController->isAuthenticated($token);

        $body = json_decode($request->getContent(), true);
        $email = strtolower($body['email']);
        try {
            $cognitoUser = $cognitoController->getCognitoUsers($email);
            if (count($cognitoUser['Users']) === 1) {
                $password = $this->usersRepository->generateUid($email);
                $url = $this->getResetLink($email, $password);
                $mailerController->sendPasswordReset($email, $url);
                return new JsonResponse(['Estado' => 'OK', 'Descripción' => 'Operación realizada correctamente!'], 202);
            }
            return new JsonResponse(['Estado' => 'Not Found', 'Descipción' => 'Usuario no registrado!'], 404);
        } catch (\Exception $exception) {
            return new JsonResponse(['Estado' => 'Bad Request', 'Descripción' => $exception], 400);
        }
    }

    /**
     * @Route("/resetPassword", name="user_reset", methods={"POST"})
     * @param Request $request
     * @param CognitoController $cognitoController
     * @param AuthenticationController $authenticationController
     * @return JsonResponse
     */
    public function resetPassword(Request $request, CognitoController $cognitoController, AuthenticationController $authenticationController)
    {
        $token = $request->headers->get('Authorization');
        $authenticationController->isAuthenticated($token);

        $body = json_decode($request->getContent(), true);
        $email = strtolower($body['email']);
        try {
            $user = $this->usersRepository->findOneBy(['email' => $email]);
            $password = $this->usersRepository->generateUid($email);
            if (!$user) {
                $user = $this->usersRepository->saveUsers($email, $password, 'reset');
            }
            $this->usersRepository->updateUsers($user, $password);
            $cognitoController->resetPassword($email, $password);
        } catch (\Exception $exception) {
            return new JsonResponse(['Estado' => 'Bad Request', 'Descripción' => $exception], 400);
        }
        return new JsonResponse(['Estado' => 'OK', 'Descripción' => 'Operación realizada correctamente!'], 202);
    }

    /**
     * @Route("/oldResetPassword", name="old_user_reset", methods={"POST"})
     * @param NewUserRequest $newUserRequest
     * @param CognitoController $cognitoController
     * @param MailerController $mailerController
     * @return JsonResponse
     */
    public function oldResetPassword(NewUserRequest $newUserRequest, CognitoController $cognitoController, MailerController $mailerController)
    {
        $cognitoUser = $cognitoController->getCognitoUsers($newUserRequest->email());
        if (count($cognitoUser['Users']) === 1) {
            $user = $this->usersRepository->findOneBy(['email' => $newUserRequest->email()]);
            $password = $this->usersRepository->generateUid($newUserRequest->email());
            if (!$user) {
                $user = $this->usersRepository->saveUsers($newUserRequest->email(), $password, 'reset');
            }
            $cognitoController->resetPassword($newUserRequest->email(), $password);
            $this->usersRepository->updateUsers($user, $password);
            return new JsonResponse(['Estado' => 'OK', 'Descripción' => 'Operación realizada correctamente!'], 202);
        }
        return new JsonResponse(['Estado' => 'BAD REQUEST', 'Descipción' => 'Usuario no registrado!'], 400);
    }

    /**
     * @Route("/addUserToGroup", name="add_user_to_group", methods={"POST"})
     * @param LoginRequest $loginRequest
     * @param CognitoController $cognitoController
     * @return JsonResponse
     */
    public function addUserToGroup(LoginRequest $loginRequest, CognitoController $cognitoController)
    {
        try {
            $addUserToGroup = $cognitoController->addUserToGroup($loginRequest->email());
            return new \Symfony\Component\HttpFoundation\JsonResponse($addUserToGroup, 200);
        } catch (\Exception $e) {
            return new JsonResponse(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Route("/createAdminUser", name="create_admin_user", methods={"POST"})
     * @param LoginRequest $loginRequest
     * @param CognitoController $cognitoController
     * @return JsonResponse
     */
    public function createAdminUser(LoginRequest $loginRequest, CognitoController $cognitoController)
    {
        try {
            $user = $cognitoController->createCognitoAdminUser($loginRequest->email(), $loginRequest->password());
            return new \Symfony\Component\HttpFoundation\JsonResponse(['aws_cognito_id' => $user['Attributes'][0]['Value']], 200);
        } catch (\Exception $e) {
            return new JsonResponse(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }


    /**
     * @Route("/checkUserGroup", name="check_user_group", methods={"POST"})
     * @param LoginRequest $loginRequest
     * @param CognitoController $cognitoController
     * @return JsonResponse
     */
    public function checkUserGroup(LoginRequest $loginRequest, CognitoController $cognitoController)
    {
        try {
            $userGroup = $cognitoController->checkUserGroup($loginRequest->email());
            return new \Symfony\Component\HttpFoundation\JsonResponse($userGroup, 200);
        } catch (\Exception $e) {
            return new JsonResponse(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }


    /**
     * @Route("/newResetPassword", name="new_user_reset", methods={"POST"})
     * @param ResetPasswordRequest $resetPasswordRequest
     * @param CognitoController $cognitoController
     * @return JsonResponse
     */
    public function newResetPassword(ResetPasswordRequest $resetPasswordRequest, CognitoController $cognitoController)
    {
        $cognitoUser = $cognitoController->getCognitoUsers($resetPasswordRequest->email());
        if (count($cognitoUser['Users']) === 1) {
            $cognitoController->newResetPassword($resetPasswordRequest->email(), $resetPasswordRequest->newPassword());
            $checkCredentialsCognitoUser = $cognitoController->checkCredentials($resetPasswordRequest->email(), $resetPasswordRequest->newPassword());
            if (!$checkCredentialsCognitoUser) {
                return new JsonResponse([
                    'user' => $resetPasswordRequest->email(),
                    'status' => 'BAD REQUEST',
                    'reason' => 'Invalid Username or Password'],
                    400
                );
            }
            $getPayload['AuthenticationResult'] = \GuzzleHttp\json_decode(json_encode($checkCredentialsCognitoUser['AuthenticationResult']), true);
            return new \Symfony\Component\HttpFoundation\JsonResponse($getPayload, 200);
        }
        return new JsonResponse(['Estado' => 'BAD REQUEST', 'Descipción' => 'Usuario no registrado!'], 400);
    }

    /**
     * @Route("/refreshToken", name="refresh_user_token", methods={"POST"})
     * @param NewTokenRequest $newTokenRequest
     * @param CognitoController $cognitoController
     * @return JsonResponse
     */
    public function refreshUserToken(NewTokenRequest $newTokenRequest, CognitoController $cognitoController)
    {
        try {
            $refreshTokenUser = $cognitoController->refreshToken($newTokenRequest->token(), $newTokenRequest->email());
            $getPayload['AuthenticationResult'] = \GuzzleHttp\json_decode(json_encode($refreshTokenUser['AuthenticationResult']), true);
            return new \Symfony\Component\HttpFoundation\JsonResponse($getPayload, 200);
        } catch (\Exception $e) {
            return new JsonResponse(['error' => $e->getMessage()], Response::HTTP_UNAUTHORIZED);
        }

    }



    /**
     * @Route("/checkResendInvitation", name="user_check_resend", methods={"POST"})
     * @param Request $request
     * @param CognitoController $cognitoController
     * @param MailerController $mailerController
     * @return JsonResponse
     */
    public function checkResendInvitation(Request $request, CognitoController $cognitoController, MailerController $mailerController)
    {
        $body = json_decode($request->getContent(), true);
        $email = $body['email'];
        try {
            $user = $this->usersRepository->findOneBy(['email' => $email]);
            if (!$user) return new JsonResponse(['status' => 'NOT_FOUND'], Response::HTTP_BAD_REQUEST);
            return new JsonResponse(['status' => 'FOUND', 'link' => $this->getLink($email, $user->getTempPassword())], Response::HTTP_OK);
        } catch (\Exception $e) {
            return new JsonResponse(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Route("/rememberInvitationEmail", name="user_remember_invitation", methods={"POST"})
     * @param Request $request
     * @param MailerController $mailerController
     * @param AuthenticationController $authenticationController
     * @return JsonResponse
     */
    public function rememberInvitationEmail(Request $request, MailerController $mailerController, AuthenticationController $authenticationController){

        $token = $request->headers->get('Authorization');
        $authenticationController->isAuthenticated($token);
        $body = json_decode($request->getContent(), true);
        try {
            $mailerController->sendRememberInvitationEmail($body['email'], $body['url']);
        } catch (\Exception $exception){
            return new JsonResponse(['status' => 'FAILED', 'message' => $exception], Response::HTTP_BAD_REQUEST);
        }
        return new JsonResponse(['status' => 'SUCCESS', 'message' => 'Email enviado con éxito'], Response::HTTP_OK);
    }

    /**
     * @Route("/getAdminResetPasswordLink", name="new_admin_reset", methods={"POST"})
     * @param ResetPasswordRequest $resetPasswordRequest
     * @param CognitoController $cognitoController
     * @return JsonResponse
     */

    public function getAdminResetPasswordLink(ResetPasswordRequest $resetPasswordRequest, CognitoController $cognitoController, FirebaseController $firebaseController)
    {

        $cognitoUser = $cognitoController->getCognitoUsers($resetPasswordRequest->email());
        if (count($cognitoUser['Users']) === 1) {
            $cognitoController->newResetPassword($resetPasswordRequest->email(), $resetPasswordRequest->newPassword());
            $checkCredentialsCognitoUser = $cognitoController->checkCredentials($resetPasswordRequest->email(), $resetPasswordRequest->newPassword());
            if (!$checkCredentialsCognitoUser) {
                return new JsonResponse([
                    'user' => $resetPasswordRequest->email(),
                    'status' => 'BAD REQUEST',
                    'reason' => 'Invalid Username or Password'],
                    400
                );
            }
            $dynamic_link = $firebaseController->getResetLink($resetPasswordRequest->email(), 'https://admin-front.i-data.com.ar/temporal_login/username/'.$resetPasswordRequest->email().'/temporal_password/'.$resetPasswordRequest->newPassword(), 'administrator', $resetPasswordRequest->newPassword());

            $getPayload['AuthenticationResult'] = \GuzzleHttp\json_decode(json_encode($checkCredentialsCognitoUser['AuthenticationResult']), true);
            return new \Symfony\Component\HttpFoundation\JsonResponse(['status' => 'OK', 'link' => $dynamic_link], 200);
        }
        return new JsonResponse(['Estado' => 'BAD REQUEST', 'Descipción' => 'Usuario no registrado!'], 400);



    }


    private function saveCoreApiUser($newUserRequest, $cognito_id, $type, $password)
    {
        $httpClient = HttpClient::create();
        $apiUsersUrl = $this->s2s_url . "/v1/api_users/symfonyAlta";
        $data = array(
            'email' => $newUserRequest->email(),
            'functional_unit_id' => $newUserRequest->functional_unit_id(),
            'role_id' => $newUserRequest->role_id(),
            'aws_cognito_id' => $cognito_id,
            'aws_cognito_username' => $newUserRequest->email(),
            'isNew' => $type,
            'url' => $this->getLink($newUserRequest->email(), $password)
        );
        return $httpClient->request('POST', $apiUsersUrl, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Authorization' => $this->s2s_token
            ],
            'body' => json_encode($data)
        ]);
    }

    public function getLink($email, $password)
    {
        return $this->front_url . '/ingreso_temporal/username/' . $email . '/tempassword/' . $password;
    }

    public function getResetLink($email, $password)
    {
        return $this->front_url . '/reset_temporal/username/' . $email . '/tempassword/' . $password;
    }

    /**
     * @Route("/registerBatch", name="users_register", methods={"POST"})
     * @return  JsonResponse
     * @throws TransportExceptionInterface
     */
    public function UserRegister(BatchUserRequest $batchUserRequest, CognitoController $cognitoController,  Request $request, AuthenticationController $authenticationController): \Symfony\Component\HttpFoundation\JsonResponse
    {
        $authenticationController->isAuthenticated($request->headers->get('Authorization'));
        $result = [];

        $arrayToWalk = $batchUserRequest->data();

        array_walk($arrayToWalk, function($array) use ($cognitoController, &$result) {
            $localDbUser = $this->usersRepository->findOneBy(['email' => $array['email']]);

            $password = $this->usersRepository->generateUid($array['email']);

            if (!($localDbUser)) {
                $this->usersRepository->SaveUsers($array['email'],$password, 'reinvite', null);
            }


            $cognitoUser = json_decode(json_encode($cognitoController->getCognitoUsers($array['email'])), true); // Deberia traer Aws/result

            if(count($cognitoUser) !== 0){
                $cognitoUser = json_decode(json_encode($cognitoController->fixMethodCreateCognitoUser($array['email'], $password)), true);
                return array_push($result, [
                    'user' => $array['email'],
                    'old_id' => $array['old_id'],
                    'status' => 'CREATED',
                    'data' => [
                        'aws_cognito_id' => $cognitoUser['Attributes'][0]['Value'],
                        'aws_cognito_username' => $cognitoUser['Username'],
                        'aws_cognito_created' => $cognitoUser['UserCreateDate'],
                        'aws_cognito_modified' => $cognitoUser['UserLastModifiedDate'],
                    ]
                ]);
            }

            return array_push($result, [
                'user' => $array['email'],
                'old_id' => $array['old_id'],
                'status' => 'TO_CHECK',
            ]);
        });

        $apiUsersUrl = $this->s2s_url . "/v1/api_users/updateCognitoUsers";

        $httpClient = HttpClient::create();
        $coreResponse = $httpClient->request('POST', $apiUsersUrl, [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Authorization' => $this->s2s_token
            ],
            'body' => ($result)
        ]);


        return $result;

    }

    /**
     * @Route("/login", name="users_login", methods={"POST"})
     * @return  JsonResponse
     */
    public function UserLogin(LoginRequest $loginRequest, Request $request, CognitoController $cognitoController): \Symfony\Component\HttpFoundation\JsonResponse
    {
        $cognitoUser = json_decode(json_encode($cognitoController->getCognitoUsers($loginRequest->email())->get('Users')), true);
        if(count($cognitoUser) === 0){
            return new JsonResponse([
                'user' => $loginRequest->email(),
                'status' => 'NOT FOUND',
                'reason' => 'User not found',
                'code' => 'UserNotFoundException'],
                404
            );
        }
        $status = $cognitoUser['0']['UserStatus'];
        if ($status == 'FORCE_CHANGE_PASSWORD') {
            return new JsonResponse([
                'user' => $loginRequest->email(),
                'status' => 'BAD REQUEST',
                'reason' => 'User is not confirmed from cognito',
                'code' => $status],
                400
            );
        }

        $checkCredentialsCognitoUser = $cognitoController->checkCredentials($loginRequest->email(), $loginRequest->password());
        if (!$checkCredentialsCognitoUser) {
            return new JsonResponse([
                'user' => $loginRequest->email(),
                'status' => 'BAD REQUEST',
                'reason' => 'Invalid Username or Password',
                'code' => 'InvalidCredentialsException'],
                400
            );
        }
        $getPayload['AuthenticationResult'] = \GuzzleHttp\json_decode(json_encode($checkCredentialsCognitoUser['AuthenticationResult']), true);
        return new \Symfony\Component\HttpFoundation\JsonResponse($getPayload, 200);
    }

    /**
     * @Route("/logout", name="users_logout", methods={"POST"})
     * @return  JsonResponse
     */
    public function UserLogout(NewTokenRequest $newTokenRequest, ApiUsersPushTokensController $apiUsersPushTokensController): \Symfony\Component\HttpFoundation\JsonResponse
    {

        $expireToken = $apiUsersPushTokensController->expireAccessToken($newTokenRequest);
        if ($expireToken) {
            return new \Symfony\Component\HttpFoundation\JsonResponse([
                'status' => 'OK',
                'message' => 'Usuario expirado exitosamente'],
                200);
        } else {
                return new \Symfony\Component\HttpFoundation\JsonResponse([
                    'status' => 'BAD_REQUEST',
                    'message' => 'El token enviado no es válido'],
                    400);
        }
    }

    /**
     * @Route("/find/{email}", name="users_find", methods={"GET"})
     * @return JsonResponse
     */
    public function findCognitoUser($email, CognitoController $cognitoController): \Symfony\Component\HttpFoundation\JsonResponse
    {
        $cognitoUser = $cognitoController->getCognitoUsers($email);
        if (!count($cognitoUser)) {
            return new \Symfony\Component\HttpFoundation\JsonResponse([
                'status' => 'NOT_FOUND',
                'message' => 'No se encontró el usuario'],
                404);
        } else {
            return new \Symfony\Component\HttpFoundation\JsonResponse([
                'status' => 'OK',
                'message' => 'El usuario es un usuario válido'],
                200);
        }
    }

    /**
     * @Route("/find/temporary_password/{email}", name="users_find_temporary_password", methods={"GET"})
     * @return JsonResponse
     */

    public function returnTemporaryPassword($email, CognitoController $cognitoController): \Symfony\Component\HttpFoundation\JsonResponse
    {
        $user = $this->usersRepository->findOneBy(['email' => $email]);
        $password = $this->usersRepository->generateUid($email);

        if (!$user) {
            $user = $this->usersRepository->saveUsers($email, $password, 'reset');
        }
        $this->usersRepository->updateUsers($user, $password);

        $cognitoController->resetPassword($email, $password);
        return new \Symfony\Component\HttpFoundation\JsonResponse([
            'status' => 'OK',
            'temporary_password' => $password],
            200);
    }

}
