<?php

namespace App\Controller;

use Aws\Exception\AwsException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Aws\Result;
use Aws\CognitoIdentityProvider\CognitoIdentityProviderClient;
use Aws\Credentials\Credentials;

class CognitoController extends AbstractController
{
    private $client;
    private $poolId;
    private $clientId;
    private $awsSecret;
    public function __construct(
        string $poolId,
        string $clientId,
        string $aws_key,
        string $aws_secret,
        string $region = 'us-east-1',
        string $version = 'latest'
    ) {
        $awsCredentials = new Credentials($aws_key, $aws_secret);
        $this->client = CognitoIdentityProviderClient::factory([
            'region' => $region,
            'version' => $version,
            'credentials'=> $awsCredentials
        ]);
        $this->awsSecret = $aws_secret;
        $this->poolId = $poolId;
        $this->clientId = $clientId;
    }

    public function createCognitoUser($email, $password)
    {
        try {
            return $this->client->adminCreateUser([
                'ClientId' => $this->clientId,
                'UserPoolId' => $this->poolId,
                'ForceAliasCreation' => true,
                'MessageAction' => 'SUPPRESS',
                'Username' => $email,
                'TemporaryPassword' => $password,
                'UserAttributes' => [
                    [
                        'Name' => 'name',
                        'Value' => $email
                    ],
                    [
                        'Name' => 'email',
                        'Value' => $email
                    ]
                ],
            ]);
        } catch (AwsException $e) {
            return $e->getMessage();
        }
    }

    public function createCognitoAdminUser($email, $password)
    {
        try {
            return $this->client->adminCreateUser([
                'ForceAliasCreation' => true,
                'MessageAction' => 'SUPPRESS',
                'TemporaryPassword' => $password,
                'UserPoolId' => $this->poolId,
                'Username' => $email,
                'UserAttributes' => [
                    [
                        'Name' => 'email',
                        'Value' => $email
                    ],
                ],
            ])->get('User');
        } catch (AwsException $e) {
            return $e->getMessage();
        }
    }


    public function getCognitoUsers($email) : ?\Aws\Result
    {
        return $this->client->listUsers([
            'UserPoolId' => $this->poolId,
            'Filter'     => "email=\"" . $email . "\""
        ]);
    }

    public function checkCredentials($username, $password)
    {
        try {
            return $this->client->adminInitiateAuth([
                'UserPoolId'     => $this->poolId,
                'ClientId'       => $this->clientId,
                'AuthFlow'       => 'ADMIN_NO_SRP_AUTH',
                'AuthParameters' => [
                    'USERNAME' => $username,
                    'PASSWORD' => $password
                ]
            ]);
        } catch (AwsException $exception) {
            return  false;
        }
    }


    public function refreshToken($refreshToken, $email): Result
    {
            return $this->client->adminInitiateAuth([
                'AuthFlow' => 'REFRESH_TOKEN_AUTH',
                'AuthParameters' => [
                    'REFRESH_TOKEN' => $refreshToken,
                    'SECRET_HASH' => $this->cognitoSecretHash($email)
                ],
                'UserPoolId'     => $this->poolId,
                'ClientId' => $this->clientId,
            ]);
    }

    public function addUserToGroup(string $username)
    {
        try {
            return $this->client->adminAddUserToGroup([
                'GroupName' => 'idata-suite-administrations', // REQUIRED
                'UserPoolId' => $this->poolId, // REQUIRED
                'Username' => $username, // REQUIRED
            ]);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
    public function checkUserGroup(string $username)
    {
        try {
            return $this->client->adminAddUserToGroup([
                'UserPoolId' => $this->poolId, // REQUIRED
                'Username' => $username, // REQUIRED
            ])->get('Groups')[0];
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }


    public function cognitoSecretHash($username){
        $hash = hash_hmac(
            'sha256',
            $username. $this->clientId,
            $this->awsSecret,
            true
        );
        return base64_encode($hash);
    }

    public function getPoolMetadata() : array
    {
        $result = $this->client->describeUserPool([
            'UserPoolId' => $this->poolId,
        ]);
        return $result->get('UserPool');
    }

    public function getPoolUsers() : array
    {
        $result = $this->client->listUsers([
            'UserPoolId' => $this->poolId,
        ]);
        return $result->get('Users');
    }

    public function confirmSignup(string $username, string $code) : string
    {
        try {
            $this->client->confirmSignUp([
                'ClientId' => $this->clientId,
                'Username' => $username,
                'ConfirmationCode' => $code,
            ]);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return '';
    }

    public function sendPasswordResetMail(string $username) : string
    {
        try {
            $this->client->adminResetUserPassword([
                'ClientId' => $this->clientId,
                'UserPoolId' => $this->poolId,
                'Username' => $username
            ]);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return '';
    }

    public function resetPassword(string $username, $password) : string
    {
        try {
            $this->client->adminSetUserPassword([
                'Password' => $password,
                'Permanent' => false,
                'UserPoolId' => $this->poolId,
                'Username' => $username,
            ]);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return '';
    }

    public function newResetPassword(string $username, $password) : string
    {
        try {

            $this->client->adminSetUserPassword([
                'Password' => $password,
                'Permanent' => true,
                'UserPoolId' => $this->poolId,
                'Username' => $username,
            ]);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return '';
    }



    public function CognitoDelete($email)
    {
        return $this->client->adminDeleteUser([
            'UserPoolId' => $this->poolId,
            'Username' => $email,
        ]);
    }

}
