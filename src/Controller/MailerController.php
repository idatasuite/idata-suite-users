<?php

namespace App\Controller;

use Aws\Credentials\Credentials;
use Aws\Exception\AwsException;
use Aws\Ses\SesClient;
use GuzzleHttp\Exception\GuzzleException;
use http\Client;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class MailerController extends AbstractController
{
    private $emailclient;

    public function __construct($email_sender,
                                $aws_email_client, $aws_email_secret,
                                string $region = 'us-east-1',
                                string $version = 'latest')
    {
        $this->sender = $email_sender;
        $awsCredentials = new Credentials($aws_email_client, $aws_email_secret);
        $this->emailclient = SesClient::factory([
            'region' => $region,
            'version' => $version,
            'credentials' => $awsCredentials
        ]);
    }

    private function sendEmail($data)
    {
         try{
            $this->emailclient->sendTemplatedEmail([
            'Destination' => [ // REQUIRED
                'ToAddresses' => [$data['email']],
            ],
            'Tags' => [
                [
                    'Name' => 'hola', // REQUIRED
                    'Value' => 'hola', // REQUIRED
                ],
            ],
            'ReplyToAddresses' => [getenv('REPLY_TO_ADDRESS')],
            'Source' => $this->sender, // REQUIRED
            'Template' => $data['email_type'], // REQUIRED
            "TemplateData" => json_encode($data['templateData'])
        ]);
            return "Mensaje enviado correctamente!";
        } catch (AwsException $exception){
            return new \Exception($exception->getAwsErrorMessage());
        }
    }

    public function sendNotificationsApiEmail($data)
    {
        $httpClient = HttpClient::create();
        $notificationsUrl = getenv('NOTIFICATIONSV2_URL') . '/notifications/sendNotification';
        try {
            return $httpClient->request('POST', $notificationsUrl, [
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',
                ],
                'body' => json_encode($data)
            ]);
        } catch (TransportExceptionInterface $e) {
            return $e->getMessage();
        }
    }

    public function sendPasswordReset($email,$url)
    {
        $data = array(
            'email'=> $email,
            'email_type' => 'reset',
            'templateData' => array (
                'url' => $url
            )
        );
        return $this->sendNotificationsApiEmail($data);
    }

    public function sendRememberInvitationEmail($email,$url)
    {
        $data = array(
            'email'=> $email,
            'email_type' => 'reinvite',
            'templateData' => array (
                'url' => $url
            )
        );
        return $this->sendNotificationsApiEmail($data);
    }
}
