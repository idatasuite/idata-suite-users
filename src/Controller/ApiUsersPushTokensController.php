<?php

namespace App\Controller;

use App\Entity\ApiUsersToken;
use App\Validator\NewTokenRequest;
use Doctrine\ORM\EntityManagerInterface;
use Firebase\JWT\JWT;
use phpseclib\Crypt\RSA;
use phpseclib\Math\BigInteger;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class ApiUsersPushTokensController extends AbstractController
{
    private $poolId;

    public function __construct(
        EntityManagerInterface $entityManager,
        $poolId,
        $region = 'us-east-1'
    )
    {
        $this->repository = $entityManager->getRepository(ApiUsersToken::class);

        $this->region = $region;
        $this->poolId = $poolId;
    }

    /**
     * @Route("/token/get_session", name="token_check_session", methods={"POST"})
     * @return  JsonResponse
     */
    public  function verifyAccessToken(NewTokenRequest $newTokenRequest)
    {
        $publicKey = null;
        $kid = static::getKid($newTokenRequest->token());
        if ($kid) {
            $row = $this->repository->findOneBy(['kid' => $kid]);
            if ($row) {
                $publicKey = $row->public_key;
            } else {
                $publicKey = static::getPublicKey($kid, $this->region, $this->poolId);
                $this->repository->saveUserToken($kid, $publicKey);
            }
        }

        if (!$publicKey) {
            return new JsonResponse([
                'status' => 'BAD REQUEST',
                'reason' => 'Malformed or expired token'
            ], 400);
        }
        return new JsonResponse(JWT::decode($newTokenRequest->token(), $publicKey, array('RS256')), 200);

    }

    public  function expireAccessToken(NewTokenRequest $newTokenRequest)
    {
        $publicKey = null;
        $kid = static::getKid($newTokenRequest->token());
        if ($kid) {
            $row = $this->repository->findOneBy(['kid' => $kid]);
            if ($row) {
                $publicKey = $row->public_key;
            } else {
                $publicKey = static::getPublicKey($kid, $this->region, $this->poolId);
                $this->repository->saveUserToken($kid, $publicKey);
            }
        }
        if ($publicKey) {
            $publicKey->expire();
            return true;
        }
        return false;
    }


    public static function getKid($jwt)
    {
        $tks = explode('.', $jwt);
        if (count($tks) === 3) {
            $header = JWT::jsonDecode(JWT::urlsafeB64Decode($tks[0]));
            if (isset($header->kid)) {
                return $header->kid;
            }
        }
        return new JsonResponse([
            'status' => 'BAD REQUEST',
            'reason' => 'Malformed or expired token'
        ], 400);
    }

    public static function getPublicKey($kid, $region, $userPoolId)
    {
        //Get jwks from URL, because the jwks may change.
        $jwksUrl = sprintf('https://cognito-idp.%s.amazonaws.com/%s/.well-known/jwks.json', $region, $userPoolId);
        $ch = curl_init($jwksUrl);
        curl_setopt_array($ch, [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 3,
        ]);
        $jwks = curl_exec($ch);
        if ($jwks) {
            $json = json_decode($jwks, false);
            if ($json && isset($json->keys) && is_array($json->keys)) {
                foreach ($json->keys as $jwk) {
                    if ($jwk->kid === $kid) {
                        return static::jwkToPem($jwk);
                    }
                }
            }
        }
        return new JsonResponse([
            'status' => 'BAD REQUEST',
            'reason' => 'Malformed or expired token'
        ], 400);
    }

    public static function jwkToPem($jwk)
    {
        if (isset($jwk->e) && isset($jwk->n)) {
            $rsa = new RSA();
            $rsa->loadKey([
                'e' => new BigInteger(JWT::urlsafeB64Decode($jwk->e), 256),
                'n' => new BigInteger(JWT::urlsafeB64Decode($jwk->n), 256)
            ]);
            return $rsa->getPublicKey();
        }
        return new JsonResponse([
            'status' => 'BAD REQUEST',
            'reason' => 'Malformed or expired token'
        ], 400);
    }

}
