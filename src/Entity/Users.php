<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UsersRepository")
 */
class Users
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $temp_password;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $functional_unit_id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $type_email;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $modified_at;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getTempPassword(): ?string
    {
        return $this->temp_password;
    }

    public function setTempPassword(string $temp_password): self
    {
        $this->temp_password = $temp_password;

        return $this;
    }

    public function getFunctionalUnitId(): ?string
    {
        return $this->functional_unit_id;
    }

    public function setFunctionalUnitId(?string $functional_unit_id): self
    {
        $this->functional_unit_id = $functional_unit_id;

        return $this;
    }

    public function getTypeEmail(): ?string
    {
        return $this->type_email;
    }

    public function setTypeEmail(?string $type_email): self
    {
        $this->type_email = $type_email;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modified_at;
    }

    public function setModifiedAt(?\DateTimeInterface $modified_at): self
    {
        $this->modified_at = $modified_at;

        return $this;
    }
}
