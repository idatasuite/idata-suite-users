<?php

namespace App\Repository;

use App\Entity\ApiUsersToken;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ApiUsersToken|null find($id, $lockMode = null, $lockVersion = null)
 * @method ApiUsersToken|null findOneBy(array $criteria, array $orderBy = null)
 * @method ApiUsersToken[]    findAll()
 * @method ApiUsersToken[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */

class ApiUsersTokenRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, ApiUsersToken::class);
        $this->manager = $manager;
    }


    public function saveUserToken($kid, $publicKey)
    {
        $created_at = new \DateTime();
        $newToken = new ApiUsersToken();

        $newToken
            ->setKid($kid)
            ->setPublicKey($publicKey)
            ->setCreatedAt($created_at);

        $this->manager->persist($newToken);
        $this->manager->flush($newToken);

        return $newToken;
    }


    // /**
    //  * @return ApiUsersToken[] Returns an array of ApiUsersToken objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ApiUsersToken
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
