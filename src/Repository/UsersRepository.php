<?php

namespace App\Repository;

use App\Entity\Users;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use PascalDeVink\ShortUuid\ShortUuid;

/**
 * @method Users|null find($id, $lockMode = null, $lockVersion = null)
 * @method Users|null findOneBy(array $criteria, array $orderBy = null)
 * @method Users[]    findAll()
 * @method Users[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UsersRepository extends ServiceEntityRepository
{
    private $manager;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, Users::class);
        $this->manager = $manager;
    }

    function generateUid($email)
    {
        try {
            $uuid = Uuid::uuid5(Uuid::NAMESPACE_URL, $email);
            $shortUuid = new ShortUuid();
            $password = $shortUuid->encode($uuid);
            // Cognito requiere claves alfanumericas, por lo forzamos un 1 al final del string generado cuando no lo es.
            if(!ctype_alpha($password)){
                return $password;
            } else {
                return substr_replace($password,1,-1,1);
            }
        } catch (\Exception $e) {
            throw new NotFoundHttpException('Uuid no se pudo resolver!');
        }
    }

    public function SaveUsers($email, $password, $email_type, $functional_unit_id = null)
    {
        $created_at = new \DateTime();
        $newUsers = new Users();
        $newUsers
            ->setEmail($email)
            ->setTempPassword($password)
            ->setCreatedAt($created_at)
            ->setFunctionalUnitId($functional_unit_id)
            ->setTypeEmail($email_type);
        $this->manager->persist($newUsers);
        $this->manager->flush($newUsers);
        return $newUsers;
    }

    public function updateUsers(Users $user, $password): Users
    {
        $email_type = "reset";
        if (empty($password))
        {
            $password = $this->generateUid($user['email']);
        }
        $user->setTempPassword($password);
        $modified_at = new \DateTime();
        $user->setModifiedAt($modified_at);
        $user->setTypeEmail($email_type);
        $this->manager->persist($user);
        $this->manager->flush();
        return $user;
    }

    public function removeUsers(Users $user)
    {
        $this->manager->remove($user);
        $this->manager->flush();
        return $user;
    }

}

