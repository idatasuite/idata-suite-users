<?php
namespace App\Validator;

class LoginRequest implements RequestDTOInterface
{
    /**
     * @var string
     */
    private $email;
    /**
     * @var string
     */
    private $password;


    public function __construct($request)
    {

        $data = json_decode($request->getContent(), true);
        $this->email = $data['Username'] ?? '';
        $this->password = $data['Password'] ?? '';

    }

    public function email(): string
    {
        return $this->email;
    }
    public function password(): string
    {
        return $this->password;
    }

}
