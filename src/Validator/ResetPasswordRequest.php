<?php
namespace App\Validator;
use Symfony\Component\Validator\Constraints as Assert;

class ResetPasswordRequest implements RequestDTOInterface
{
    /**
     * @Assert\NotBlank()
     */
    private $email;
    /**
     * @var string
     */
    private $old_password;
    /**
     * @var string
     */
    private $new_password;

    public function __construct($request)
    {

        $data = json_decode($request->getContent(), true);
        $this->email = $data['email'] ?? '';
        $this->old_password = $data['old_password'] ?? '';
        $this->new_password = $data['new_password'] ?? '';
    }

    public function email(): string
    {
        return $this->email;
    }

    public function oldPassword()
    {
        return $this->old_password;
    }

    public function newPassword()
    {
        return $this->new_password;
    }


}
