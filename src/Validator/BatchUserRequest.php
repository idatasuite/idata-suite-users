<?php

namespace App\Validator;

class BatchUserRequest extends \ArrayObject implements RequestDTOInterface
{
    /**
     * @var array
     */
    private $data;

    public function __construct($request) {
        $data = json_decode($request->getContent(), true);
        $this->data = $data['data'] ?? '';
    }

    public function data(): array
    {
        return $this->data;
    }

}
