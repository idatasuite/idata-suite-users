<?php
namespace App\Validator;

class NewTokenRequest implements RequestDTOInterface
{
    /**
     * @var string
     */
    private $token;
    /**
     * @var string
     */
    private $email;

    public function __construct($request)
    {

        $data = json_decode($request->getContent(), true);
        $this->token = $data['token'] ?? '';
        $this->email = $data['email'] ?? '';

    }

    public function token(): string
    {
        return $this->token;
    }
    public function email(): string
    {
        return $this->email;
    }

}
