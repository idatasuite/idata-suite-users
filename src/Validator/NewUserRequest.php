<?php
namespace App\Validator;
use Symfony\Component\Validator\Constraints as Assert;

class NewUserRequest implements RequestDTOInterface
{
    /**
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;
    /**
     * @var int
     */
    private $functional_unit_id;
    /**
     * @var int
     */
    private $role_id;
    /**
     * @var string
     */
    private $type;

    public $url ='';

    public function __construct($request)
    {

        $data = json_decode($request->getContent(), true);
        $this->email = $data['email'] ?? '';
        $this->functional_unit_id = $data['functional_unit_id'] ?? '';
        $this->role_id = $data['role_id'] ?? '';
        $this->type = $data['type'] ?? '';
    }

    public function email(): string
    {
        return $this->email;
    }

    public function functional_unit_id()
    {
        return $this->functional_unit_id;
    }

    public function role_id()
    {
        return $this->role_id;
    }

    public function type()
    {
        return $this->type;
    }

}
